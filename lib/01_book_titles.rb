class Book
  # TODO: your code goes here!
  attr_reader :title
  def initialize(title = '')
    @title = title
  end

  def title=(title)
    exceptions = %w[the in and a an and of]
    @title = title.split.map.with_index do |word, index|
      if (exceptions.include? word) && !index.zero?
        word
      else
        word.capitalize
      end
    end.join(' ')

  end

end
