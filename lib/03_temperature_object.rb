class Temperature
  def initialize(options)
    t = options[:c]
    @temperature = t ? t : (options[:f] - 32) / 9.0 * 5.0
  end

  def in_celsius
    @temperature
  end

  def in_fahrenheit
    (@temperature * 9.0 / 5.0) + 32
  end

  def self.from_celsius(temp)
    return Temperature.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    return Temperature.new(f: temp)
  end
end

class Celsius < Temperature
  def initialize(c_temp)
    super(c: c_temp)
  end
end

class Fahrenheit < Temperature
  def initialize(f_temp)
    super(f: f_temp)
  end
end
