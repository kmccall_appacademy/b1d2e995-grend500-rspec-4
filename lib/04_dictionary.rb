# Dictionary class
require 'byebug'
class Dictionary

  attr_reader :entries

  def initialize
    @entries = Hash.new()
  end

  def add(new_entry)
    new_entry.is_a?(Hash) ? @entries.merge!(new_entry) : @entries[new_entry] = nil
  end

  def include?(poss_entry)
    @entries.include? poss_entry
  end

  def keywords
    @entries.keys.sort
  end

  def find(entry)
    @entries.select { |k, _v| k.start_with? entry }
  end

  def printable
    keywords.reduce("") do |output, key|
      output += "[#{key}] \"#{@entries[key]}\"\n"
    end[0..-2]
  end
end
