class Timer
  attr_accessor :seconds
  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    mins = (@seconds % 3600) / 60
    secs = @seconds % 60

    "#{padded(hours)}:#{padded(mins)}:#{padded(secs)}"
  end

  def padded(num)
    num > 10 ? num.to_s : '0' + num.to_s
  end
end
